### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 3cf79e02-b429-11ec-3f16-b508758ab60e
begin
	using Pkg
	Pkg.add("Images")
	Pkg.add("DataFrames")
	Pkg.add("ImageMetadata")
	Pkg.add("MLDataUtils")
	Pkg.add("Flux")
	Pkg.add("Plots")
	using Images
	using DataFrames
	using ImageMetadata
	using MLDataUtils
	using Plots
	using Flux: @epochs
	using Flux
end

# ╔═╡ 9102550b-8b64-44a7-aef7-2e49d93d25a5
# this function creates minibatches of the data. Each minibatch is a tuple of (data, labels).
function make_minibatch(X, Y, idxs)
    X_batch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        X_batch[:, :, :, i] = Float32.(X[idxs[i]])
    end
    Y_batch = onehotbatch(Y[idxs], 0:1)
    return (X_batch, Y_batch)
end

# ╔═╡ 95d2da72-a8d8-4868-b727-94093c14c0e8
begin
    # here we define the train and test sets.
    batchsize = 128
	    train_set = make_minibatch(x_train, y_train, i);
    test_set = make_minibatch(x_test, y_test, 1:length(x_test));
end;

# ╔═╡ 7f0b5dc4-5e07-4269-a305-665422ca4a4d
function build_model(; imgsize=(28,28,1), nclasses=10)
    return Chain(
 	    Dense(prod(imgsize), 32, relu),
            Dense(32, nclasses))
end

# ╔═╡ 20200b2a-01e5-4013-85f0-b5c4f7e6bda4
function loss_all(dataloader, model)
    l = 0f0
    for (x,y) in dataloader
        l += logitcrossentropy(model(x), y)
    end
    l/length(dataloader)
end

# ╔═╡ 55d65902-ab5e-4b83-9e3b-d7d7ce136b98
function accuracy(data_loader, model)
    acc = 0
    for (x,y) in data_loader
        acc += sum(onecold(cpu(model(x))) .== onecold(cpu(y)))*1 / size(x,2)
    end
    acc/length(data_loader)
end

# ╔═╡ 11991fed-2841-4f10-9bff-22218372efeb


# ╔═╡ 12abb981-c436-4453-8338-6e4348ef8ddd
function resize_and_grayify(directory, im_name, width::Int64, height::Int64)
    resized_gray_img = Gray.(load(directory * "/" * im_name)) |> (x -> imresize(x, width, height))
    try
        save("preprocessed_" * directory * "/" * im_name, resized_gray_img)
    catch e
        if isa(e, SystemError)
            mkdir("preprocessed_" * directory)
            save("preprocessed_" * directory * "/" * im_name, resized_gray_img)
        end
    end
end

# ╔═╡ ce8e6046-6747-4638-b197-af8e06911b57
function process_images(directory, width::Int64, height::Int64)
    files_list = readdir(directory)
    map(x -> resize_and_grayify(directory, x, width, height),                               files_list)
end

# ╔═╡ 1ec67b04-a3d4-4ef3-9aee-d10b2e0ed21b
function getdata(args)
    ENV["DATADEPS_ALWAYS_ACCEPT"] = "true"

    # Loading Dataset	
    xtrain, ytrain = process_images("/home/miracle/.julia/chest_xray/train")
    xtest, ytest = process_images("/home/miracle/.julia/chest_xray/test")
	
    # Reshape Data in order to flatten each image into a linear array
    xtrain = Flux.flatten(xtrain)
    xtest = Flux.flatten(xtest)

    # One-hot-encode the labels
    ytrain, ytest = onehotbatch(ytrain, 0:9), onehotbatch(ytest, 0:9)

    # Batching
    train_data = DataLoader((xtrain, ytrain), batchsize=args.batchsize, shuffle=true)
    test_data = DataLoader((xtest, ytest), batchsize=args.batchsize)

    return train_data, test_data
end

# ╔═╡ c59e02bc-091f-4ed0-9d63-e6c459f18307
n_resolution = 90

# ╔═╡ 99a90073-0f3b-453f-aece-8509223245b4
begin
    process_images("/home/miracle/.julia/chest_xray/test/NORMAL/", n_resolution, n_resolution)
    process_images("/home/miracle/.julia/chest_xray/test/NORMAL/", n_resolution, n_resolution)
    process_images("/home/miracle/.julia/chest_xray/test/PNEUMONIA/", n_resolution, n_resolution)
    process_images("/home/miracle/.julia/chest_xray/test/PNEUMONIA/", n_resolution, n_resolution)
end

# ╔═╡ c13bab02-f38a-4e4c-8a38-27b8ae3941c9
begin
    train_loss = Float64[]
    test_loss = Float64[]
    acc = Float64[]
    ps = Flux.params(process_images)
    opt = ADAM()
    L(x, y) = Flux.crossentropy(model(x), y)
    L((x,y)) = Flux.crossentropy(model(x), y)
    accuracy(x, y, f) = mean(Flux.onecold(f(x)) .== Flux.onecold(y))
    
    function update_loss!()
        push!(train_loss, mean(L.(train_set)))
        push!(test_loss, mean(L(test_set)))
        push!(acc, accuracy(test_set..., model))
        @show("train loss = %.2f, test loss = %.2f, accuracy = %.2f\n", train_loss[end], test_loss[end], acc[end])
    end
end

# ╔═╡ 56c458f5-bd20-464d-9fc6-c7de93d2c500
function train(; kws...)
    # Initializing Model parameters 
    args = Args(; kws...)

    # Load Data
    train_data,test_data = getdata(args)

    # Construct model
    m = build_model()
    train_data = args.device.(train_data)
    test_data = args.device.(test_data)
    m = args.device(m)
    loss(x,y) = logitcrossentropy(m(x), y)
    
    ## Training
    evalcb = () -> @show(loss_all(train_data, m))
    opt = ADAM(args.η)
		
    @epochs args.epochs Flux.train!(loss, params(m), train_data, opt, cb = evalcb)

    @show accuracy(train_data, m)

    @show accuracy(test_data, m)
end

# ╔═╡ d3f6fd27-8dc1-4ae1-92f4-75a277baf5dd
begin
    plot(train_loss, xlabel="Iterations", title="Model Training", label="Train loss", lw=2, alpha=0.9)
    plot!(test_loss, label="Test loss", lw=2, alpha=0.9)
    plot!(acc, label="Accuracy", lw=2, alpha=0.9)
end

# ╔═╡ d8712480-2b81-404e-a6d8-2f1badd4cc56
begin
    plot(train_loss, xlabel="Iterations", title="Model Training", label="Train loss", lw=2, alpha=0.9, legend = :right)
    plot!(test_loss, label="Test loss", lw=2, alpha=0.9)
    plot!(acc, label="Accuracy", lw=2, alpha=0.9)
    vline!([82], lw=2, label=false)
end

# ╔═╡ Cell order:
# ╠═3cf79e02-b429-11ec-3f16-b508758ab60e
# ╠═1ec67b04-a3d4-4ef3-9aee-d10b2e0ed21b
# ╠═99a90073-0f3b-453f-aece-8509223245b4
# ╠═9102550b-8b64-44a7-aef7-2e49d93d25a5
# ╠═95d2da72-a8d8-4868-b727-94093c14c0e8
# ╠═7f0b5dc4-5e07-4269-a305-665422ca4a4d
# ╠═20200b2a-01e5-4013-85f0-b5c4f7e6bda4
# ╠═55d65902-ab5e-4b83-9e3b-d7d7ce136b98
# ╠═11991fed-2841-4f10-9bff-22218372efeb
# ╠═56c458f5-bd20-464d-9fc6-c7de93d2c500
# ╠═12abb981-c436-4453-8338-6e4348ef8ddd
# ╠═ce8e6046-6747-4638-b197-af8e06911b57
# ╠═c59e02bc-091f-4ed0-9d63-e6c459f18307
# ╠═c13bab02-f38a-4e4c-8a38-27b8ae3941c9
# ╠═d3f6fd27-8dc1-4ae1-92f4-75a277baf5dd
# ╠═d8712480-2b81-404e-a6d8-2f1badd4cc56
